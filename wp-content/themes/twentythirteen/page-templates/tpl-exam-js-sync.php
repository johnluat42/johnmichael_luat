<?php
/**
 * Template Name: Kaplan Exam JS SYNC
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="loader">
			   <center>
				   <img class="loading-image" src="<?php bloginfo('url');?>/wp-content/themes/twentythirteen/images/preloader.gif" alt="loading..">
			   </center>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="item-list">
				<div class="category"><h3>BBC News - Home</h3></div>
				<div id="news"></div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="item-list">
				<div class="category"><h3>BBC News - Business</h3></div>
				<div id="business"></div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12">
			<div class="item-list">
				<div class="category"><h3>BBC News - Technology</h3></div>
				<div id="technology"></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function() {
	getEachSource('<?php echo RSS_BBC_TOP_NEWS; ?>', 'news');
	getEachSource('<?php echo RSS_BBC_BUSINESS; ?>', 'business');
	getEachSource('<?php echo RSS_BBC_TECHNOLOGY; ?>', 'technology');
	$("#load_the_article").click(function(e) {
		//alert(1);
		//user did not join entry
		e.preventDefault();
		
	});
	
	function getEachSource(category, pos) {
		$.ajax({
			_ajax_nonce: '<?php echo wp_create_nonce( 'my_ajax_nonce' ); ?>',
			async:false,
			dataType: 'text', 
			url: "<?php bloginfo('url');?>/wp-admin/admin-ajax.php",
			type: "POST",
			data: {
				'action': 'load_articles',
				'category' : category
			},
			success: function(data){
				successCallback(data, pos)
				
				
			},
			error: function(xhr, status) {
				alert(status);
			},
			beforeSend: function(){
				$('.loader').show()
			},
			complete: function(){
				$('.loader').hide();
			}
		});
	}
	
	function successCallback(data, pos) {
		parsedJson = $.parseJSON(data);
		var titleBuild = '';
		var descriptionBuild = '';
		$.each(parsedJson, function() {
			titleBuild += "<div class='list-article-main'><div class='image'><img src='"+ (this['media'])+ "' class='center-block img-rounded'></div><a href='"+(this['link'])+ "' target='_blank'><div class='title'><h4>"+ (this['title'])+ "</h4></div></a><div class='description'>"+ (this['description'])+ "</div><div class='publish-date'>"+ (this['date'])+ "</div><div class='spacer'>&nbsp;</div></div>";
		});
		if (pos == 'news') {
			$('#news').html(titleBuild);
		} else if (pos == 'business') {
			$('#business').html(titleBuild);
		} else {
			$('#technology').html(titleBuild);
		}
	}
	
});


</script>

<?php
get_footer();

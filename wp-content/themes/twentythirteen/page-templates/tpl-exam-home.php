<?php
/**
 * Template Name: Kaplan Exam Home
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php
	#echo "<pre>";
	$rssBBCTopNews = getRssFeed(RSS_BBC_TOP_NEWS, '');
	$rssBBCTopBusiness = getRssFeed(RSS_BBC_BUSINESS, '');
	$rssBBCTopTechnology = getRssFeed(RSS_BBC_TECHNOLOGY, '');
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="category">BBC News - Home</div>
			<?php 
			$article = '';
			foreach ( $rssBBCTopNews as $article ):?>
			<div class="item-list">
				<div class="image">
					<img src="<?php echo $article['media']; ?>" />
				</div>
				<div class="title">
					<a href="<?php echo $article['link']; ?>" target="_blank"><?php echo $article['title']; ?></a>
				</div>
				<div class="description">
					<?php echo $article['description']; ?>
				</div>
				<div class="publish-date">
					<?php echo $article['date']; ?>
				</div>
				
				<div class="spacer">&nbsp;</div>
			</div>
			<?php endforeach; ?>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="category">BBC News - Business</div>
			<?php 
			$article = '';
			foreach ( $rssBBCTopBusiness as $article ):?>
			<div class="item-list">
				<div class="image">
					<img src="<?php echo $article['media']; ?>" />
				</div>
				<div class="title">
					<a href="<?php echo $article['link']; ?>" target="_blank"><?php echo $article['title']; ?></a>
				</div>
				<div class="description">
					<?php echo $article['description']; ?>
				</div>
				<div class="publish-date">
					<?php echo $article['date']; ?>
				</div>
				
				<div class="spacer">&nbsp;</div>
			</div>
			<?php endforeach; ?>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12">
			<div class="category">BBC News - Technology</div>
			<?php 
			$article = '';
			foreach ( $rssBBCTopTechnology as $article ):?>
			<div class="item-list">
				<div class="image">
					<img src="<?php echo $article['media']; ?>" />
				</div>
				<div class="title">
					<a href="<?php echo $article['link']; ?>" target="_blank"><?php echo $article['title']; ?></a>
				</div>
				<div class="description">
					<?php echo $article['description']; ?>
				</div>
				<div class="publish-date">
					<?php echo $article['date']; ?>
				</div>
				
				<div class="spacer">&nbsp;</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php
get_footer();
